package Data;

import Common.RandomHelper;
import Constant.Constant;
import Railway.Account;

public class Data {
	
	public Account loginActivateMail = new Account(
			Constant.MAIL_USERNAME,
			Constant.MAIL_PASSWORD
	);
	
	
	public Account registerAccount = new Account(
			RandomHelper.randomEmail(10,5),
			RandomHelper.randomString(10),
			RandomHelper.randomNumbers(10)
	);
	
	public String newEmail = registerAccount.getEmail();
	public String newPassword = registerAccount.getPassword();
	
	public Account loginWithNewAccount = new Account(
			newEmail,
			newPassword
	);
}
