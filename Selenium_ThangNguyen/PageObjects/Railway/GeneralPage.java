package Railway;

import Constant.Constant;
import element_wrapper.Element;

public class GeneralPage {

	protected final Element tabMenu(String path) {return new Element("//div[@id='menu']//a[@href='"+path+"']");}
		
	public LoginPage goToLoginPage() {
		this.tabMenu(Constant.LOGIN_PAGE).click();
		return new LoginPage();
	}
	
	public MyTicketPage goToMyTicketPage() {
		this.tabMenu(Constant.MYTICKET_PAGE).click();
		return new MyTicketPage();
	}
	
	public BookTicketPage goToBookTicketPage() {
		this.tabMenu(Constant.BOOKTICKET_PAGE).click();
		return new BookTicketPage();
	}
	
	
	public RegisterPage goToRegisterPage() {
		this.tabMenu(Constant.REGISTER_PAGE).click();
		return new RegisterPage();
	}
}
