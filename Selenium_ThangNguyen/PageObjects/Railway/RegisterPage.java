package Railway;

import element_wrapper.Element;

public class RegisterPage extends GeneralPage {
	
	private final Element txtEmail = new Element("//input[@id='email']");
	private final Element txtPassword = new Element("//input[@id='password']");
	private final Element txtConfirmPassword = new Element("//input[@id='confirmPassword']");
	private final Element txtPID = new Element("//input[@id='pid']");
	private final Element btnRegister = new Element("//input[@type='submit']");
		
	public RegisterPage registerNewAccount() {
		this.btnRegister.scrollToElement();
		this.btnRegister.click();
		return this;
	}
	
	public RegisterPage enterRegisterInformation(Account account) {
		this.txtEmail.sendKeys(account.getEmail());
		this.txtPassword.sendKeys(account.getPassword());
		this.txtConfirmPassword.sendKeys(account.getPassword());
		this.txtPID.sendKeys(account.getPID());
		return this;
	}
	
}
