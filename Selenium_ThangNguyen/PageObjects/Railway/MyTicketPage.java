package Railway;

import Constant.Constant;
import element_wrapper.Element;

public class MyTicketPage extends GeneralPage {

	private final Element tblBodyRows = new Element("//table[@class='MyTable']/tbody/tr[@class != 'TableSmallHeader']");
	private final Element txtCellOfColumn(int index, String item){return new Element("//table[@class='MyTable']//tr["+(index+1)+"]/td[count(//tr[@class='TableSmallHeader']/th[text()='"+item+"']//preceding-sibling::th)+1]");}
	private final Element btnFilter = new Element("//input[@type='submit' and @value='Apply filter']");
	private final Element filterDpStation = new Element("//select[@name='FilterDpStation']");
	private final Element filterStatus = new Element("//select[@name='FilterStatus']");
	private final Element filterErrorMsg = new Element("//div[@class='error message']");
		
	public MyTicketPage filterTicketWithStatus(String status) {
		this.filterStatus.selectItemInDropDownList(status, Constant.DEFAULT_TIMEOUT);
		return this;
	}
	
	public MyTicketPage filterTicketWithDepartStation(String departStation) {
		this.filterDpStation.selectItemInDropDownList(departStation, Constant.DEFAULT_TIMEOUT);
		return this;
	}
	
	public MyTicketPage applyFilter() {
		this.btnFilter.click();
		return this;
	}
		
	public boolean isCorrectTicketFilterDisplay(String columnName, String value) {
		int rowsCount = this.tblBodyRows.getSize();
		try {
			for(int index = 1; index <= rowsCount; index++) {
				if(this.txtCellOfColumn(index, columnName).getText().equals(value)) {
					return true;
				}
			}
		}catch(Exception e){
			return false;
		}
		return true;
	}
		
	public String getFilterErrorMsg() {
		return this.filterErrorMsg.getText();
	}
}
