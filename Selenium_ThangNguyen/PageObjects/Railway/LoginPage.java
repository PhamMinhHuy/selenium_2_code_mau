package Railway;

import element_wrapper.Element;

public class LoginPage extends GeneralPage{
	
	private final Element txtUserName = new Element("//input[@id='username']");
	private final Element txtPassword = new Element("//input[@id='password']");
	private final Element btnLogin = new Element("//input[@value='login']");
		
	public LoginPage enterAccount(Account account) {
		this.txtUserName.sendKeys(account.getEmail());
		this.txtPassword.sendKeys(account.getPassword());
		return this;
	}
		
	public HomePage loginSuccess() {
		this.btnLogin.click();
		return new HomePage();
	}
		
}
