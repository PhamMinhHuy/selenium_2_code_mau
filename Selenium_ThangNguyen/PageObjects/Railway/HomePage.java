package Railway;

import ActivateMail.MailLoginPage;
import Constant.Constant;
import core.driver_wrapper.management.Driver;
import core.driver_wrapper.management.DriverManagement;

public class HomePage extends GeneralPage{

	public HomePage open() {
		DriverManagement.getDriver().navigate().to(Constant.RAILWAY_URL);
		return this;
	}
	
	public MailLoginPage openActivateMail(int tabIndex) {
		Driver.openNewTab(Constant.ACTIVATE_MAIL_URL);
		Driver.switchToSpecificTab(tabIndex);
		return new MailLoginPage();
	}
}
