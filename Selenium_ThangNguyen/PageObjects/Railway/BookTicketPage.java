package Railway;

import java.util.ArrayList;

import Constant.Constant;
import element_wrapper.Element;

public class BookTicketPage extends GeneralPage{

	private final Element dDListDepartDate = new Element("//select[@name = 'Date']");
	private final Element dDListDepartFrom = new Element("//select[@name = 'DepartStation']");
	private final Element dDListarriveAt = new Element("//select[@name = 'ArriveStation']");
	private final Element dDListseatType = new Element("//select[@name = 'SeatType']");
	private final Element btnBookTicket = new Element("//input[@type='submit']");
	
	public BookTicketPage bookTicketWithServeralTimes(ArrayList<TicketInformation> ticketInformationList, int times) {
		for(int i = 1; i <= times; i++ ) {
			ticketInformationList.add(new TicketInformation(dDListDepartDate.selectRandomValueInDropDownList(Constant.DEFAULT_TIMEOUT),
					dDListDepartFrom.selectRandomValueInDropDownList(Constant.DEFAULT_TIMEOUT),
					dDListarriveAt.selectRandomValueInDropDownList(Constant.DEFAULT_TIMEOUT),
					dDListseatType.selectRandomValueInDropDownList(Constant.DEFAULT_TIMEOUT),
					1
					));
			this.btnBookTicket.scrollToElement();
			this.btnBookTicket.click();
			this.tabMenu(Constant.BOOKTICKET_PAGE).click();
		}
		return this;
	}
	
}
