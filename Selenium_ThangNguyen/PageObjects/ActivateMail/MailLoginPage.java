package ActivateMail;

import Railway.Account;
import element_wrapper.Element;

public class MailLoginPage{
	private final Element txtUsername = new Element("//input[@id='rcmloginuser']");
	private final Element txtPassword = new Element("//input[@id='rcmloginpwd']");
	private final Element btnLogin = new Element("//button[@id='rcmloginsubmit']");
	
	public MailBoxWelcomePage loginMail(Account account) {
		this.txtUsername.sendKeys(account.getEmail());
		this.txtPassword.sendKeys(account.getPassword());
		this.btnLogin.click();	
		return new MailBoxWelcomePage();
	}
}
