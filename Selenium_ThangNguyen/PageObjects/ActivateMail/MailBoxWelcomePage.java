package ActivateMail;

import Common.Logger;
import Railway.RegisterPage;
import core.driver_wrapper.management.Driver;
import element_wrapper.Element;

public class MailBoxWelcomePage {
	
	private final Element lnkJunkMailBox = new Element("//a[@href='./?_task=mail&_mbox=Junk']");
	private final Element lnkInboxMail = new Element("//a[@href='./?_task=mail&_mbox=INBOX']");
	private final Element btnSearchOption = new Element("//a[@class='button options']");
	private final Element txtMailSearchForm = new Element("//input[@id='mailsearchform']");
	private final Element btnSearch = new Element("//button");
	private final Element mailNeedToActive(String email) {return new Element("//td[@class='subject']//span[contains(text(),'Please confirm your account "+email+"')]");}
	private final Element lnkActivation = new Element("//a[contains(text(),'http://railwaysg2.somee.com/Account/Confirm')]");
	
	public RegisterPage activateNewUserRegistered(String username, int tabIndex) {
		try {
			this.lnkInboxMail.click();
			this.btnSearchOption.click();
			this.txtMailSearchForm.sendKeys(username);
			Driver.wait(2);
			this.btnSearch.click(); 
			Driver.wait(2);
			if(this.mailNeedToActive(username).isDisplayed()) {
				this.mailNeedToActive(username).doubleClick();
				Driver.wait(2);
				this.lnkActivation.click();
				Driver.switchToSpecificTab(tabIndex);
			}else {
				this.lnkJunkMailBox.click();
				this.btnSearchOption.click();
				this.txtMailSearchForm.sendKeys(username);
				Driver.wait(2);
				this.btnSearch.click(); 
				Driver.wait(2);
				if(this.mailNeedToActive(username).isDisplayed()) {
					this.mailNeedToActive(username).doubleClick();
					Driver.wait(2);
					this.lnkActivation.click();
					Driver.switchToSpecificTab(tabIndex);
				}
				else {
					Logger.info("No mail is found!");
				}
			}
		}catch(Exception e) {
			Logger.info("Something is wrong!");
			throw e;
		}
		
		return new RegisterPage();
	}
}
