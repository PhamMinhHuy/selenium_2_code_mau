package Common;

import java.util.Random;

public class RandomHelper {

	public static String randomString(int length) {
		String alphabet = "abcdefghijklmnopqrstuvwyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		
		for(int i = 0; i < 10; i++) {
		   int index = random.nextInt(alphabet.length());
		   char randomChar = alphabet.charAt(index);
		   sb.append(randomChar);
		}

		
		String randomString = sb.toString();
		return randomString;
	}
	
	public static String randomDomain(int length) {
		String alphabet = "abcdefghijklmnopqrstuvwyz1234567890";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();		
		for(int i = 0; i < 10; i++) {
			   int index = random.nextInt(alphabet.length());
			   char randomChar = alphabet.charAt(index);
			   sb.append(randomChar);
			}
		String randomString = sb.toString();
		
		String[] domains = {"de","com","in","en","us","guru","vn","lnk","ml"};
		int idx = new Random().nextInt(domains.length);
        String domain = (domains[idx]).toString();
        
		return "@"+randomString+"."+domain;
	}

	public static String randomEmail(int stringLength, int DomainLength) {
		return randomString(stringLength)+randomDomain(DomainLength);
	}
	
	public static String randomNumbers(int length) {
		String numbers = "0123456789";
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		
		for(int i = 0; i < 10; i++) {
		   int index = random.nextInt(numbers.length());
		   char randomNumber = numbers.charAt(index);
		   sb.append(randomNumber);
		}
		String randomNumbers = sb.toString();
		return randomNumbers;
	}
	
}
