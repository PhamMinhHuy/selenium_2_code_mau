package Common;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import core.driver_wrapper.management.DriverManagement;

public class Utilities {
	
	public static String getProjectPath() {
		return System.getProperty("user.dir");
	}
	
	public static String getDateNow(String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
	    Date date = new Date();
	    return formatter.format(date);
	}
	
	public static String takeScreenShot(String filename, String filepath) throws Exception {
		String path = "";
		try {
			// Convert web driver object to TakeScreenshot
			TakesScreenshot scrShot = ((TakesScreenshot) DriverManagement.getDriver());

			// Call getScreenshotAs method to create image file
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);

			// Move image file to new destination
			File DestFile = new File(filepath + File.separator + filename + ".png");

			// Copy file at destination
			FileUtils.copyFile(SrcFile, DestFile);
			path = DestFile.getAbsolutePath();
		} catch (Exception e) {
			System.err.println("An error occurred when capturing screen shot: " + e.getMessage());
		}
		return path;
	}
	
}
