package Railway;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.ArrayList;

import org.testng.annotations.Test;

import Common.Logger;
import Data.Data;

public class FinalExamSeleniumTest extends BaseTest{
	
	@Test
	public void FTTC01() {
		Data data = new Data();
		Logger.info("FTTC01 - User can filter 'Manager ticket' table with Depart Station");
		Logger.info("1. Navigate to QA Railway Website");
		Logger.info("Pre-condition: Create and activate a new account");
		homePage.open();
		
		Logger.info("Go to Register Page");
		registerPage = homePage.goToRegisterPage();
		
		Logger.info("Register a new account");
		registerPage.enterRegisterInformation(data.registerAccount);
		registerPage.registerNewAccount();
		
		Logger.info("Open activate mail box");
		mailLoginPage = homePage.openActivateMail(1);
		
		Logger.info("Login to activate mail box");
		mailBoxWelcomePage = mailLoginPage.loginMail(data.loginActivateMail);
		
		Logger.info("Active new created account");
		mailBoxWelcomePage.activateNewUserRegistered(data.newEmail, 2);
		
		Logger.info("2. Login with valid account");
		homePage.goToLoginPage().enterAccount(data.loginWithNewAccount).loginSuccess();
		
		Logger.info("3. Book more than 6 tickets with different Depart Stations");
		bookTicketPage = homePage.goToBookTicketPage();
		ArrayList<TicketInformation> ticketInformationList = new ArrayList<>();
		bookTicketPage.bookTicketWithServeralTimes(ticketInformationList, 7);
		
		Logger.info("4. Click on 'My ticket' tab");
		myTicketPage = homePage.goToMyTicketPage();
		
		Logger.info("5. Select one of booked Depart Station in 'Depart Station' dropdownlist");
		myTicketPage.filterTicketWithDepartStation(ticketInformationList.get(1).getDepartFrom());
		
		Logger.info("6. Click 'Apply filter' button");
		myTicketPage.applyFilter();
		
		Logger.verify("'Manage ticket' table shows correct ticket(s)");
		boolean isCorrectTicketFilterDisplayed = myTicketPage.isCorrectTicketFilterDisplay("Depart Station", ticketInformationList.get(1).getDepartFrom());
		assertTrue(isCorrectTicketFilterDisplayed, "No ticket found with Depart Station: "+ticketInformationList.get(1).getDepartFrom());
	}
	
	@Test
	public void FTTC02() {
		Data data = new Data();
		Logger.info("Error displays when user applies filter with un-existed value of 'Status' in 'Manage ticket' table");
		Logger.info("1. Navigate to QA Railway Website");
		Logger.info("Pre-condition: Create and activate a new account");
		homePage.open();
		
		Logger.info("Go to Register Page");
		registerPage = homePage.goToRegisterPage();
		
		Logger.info("Register a new account");
		registerPage.enterRegisterInformation(data.registerAccount);
		registerPage.registerNewAccount();
		
		Logger.info("Open activate mail box");
		mailLoginPage = homePage.openActivateMail(1);
		
		Logger.info("Login to activate mail box");
		mailBoxWelcomePage = mailLoginPage.loginMail(data.loginActivateMail);
		
		Logger.info("Active new created account");
		mailBoxWelcomePage.activateNewUserRegistered(data.newEmail, 2);
		
		Logger.info("2. Login with valid account");
		homePage.goToLoginPage().enterAccount(data.loginWithNewAccount).loginSuccess();
		
		Logger.info("3. Book more than 6 tickets");
		bookTicketPage = homePage.goToBookTicketPage();
		ArrayList<TicketInformation> ticketInformationList = new ArrayList<>();
		bookTicketPage.bookTicketWithServeralTimes(ticketInformationList, 7);
		
		Logger.info("4. Click on 'My ticket' tab");
		myTicketPage = homePage.goToMyTicketPage();
		
		Logger.info("5. Select 'Paid' for 'Status'");
		myTicketPage.filterTicketWithStatus("Paid");
		
		Logger.info("6. Click 'Apply filter' button");
		myTicketPage.applyFilter();
		
		Logger.verify("'Manage ticket' table shows message 'Sorry, can't find any results that match your filters. Please change the filters and try again.'");
		String actualMsg = myTicketPage.getFilterErrorMsg();
		String expectedMsg = "Sorry, can't find any results that match your filters.\nPlease change the filters and try again.";
		assertEquals(actualMsg, expectedMsg, "The error message should be displayed");
	}
}
