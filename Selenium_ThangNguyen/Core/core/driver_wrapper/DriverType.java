package core.driver_wrapper;

public enum DriverType {

	Chrome("Chrome"), Firefox("Firefox");
	
	private final String value;
	
	// Instantiates a new driver type.
	DriverType(String value) {
		this.value = value;
	}
	
	// Gets the value.
	public String getValue() {
		return value;
	}
}
